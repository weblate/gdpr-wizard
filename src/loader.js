/*
This file is part of LiberaForms

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import i18n from '@/i18n';
import GDRPWizard from "@/components/Wizard.vue";

document.querySelectorAll("[vue-component=gdpr-wizard]")
        .forEach((element) => {
            createApp(GDRPWizard).use(i18n)
                                 .use(createPinia())
                                 .provide('ui_language', element.dataset.ui_language)
                                 .provide('csrf_token', element.dataset.csrf_token)
                                 .provide('data_endpoint', element.dataset.data_endpoint)
                                 .provide('org_endpoint', element.dataset.org_endpoint)
                                 .provide('consent_id', element.dataset.consent_id)
                                 .provide('consent_url', element.dataset.consent_url)
                                 .provide('data_law', element.dataset.data_law)
                                 .provide('fallback_consent_language', element.dataset.fallback_consent_language)
                                 .mount(element)
});
